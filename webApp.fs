(* all the code for the webapp shared between the local dotnet core app and the cloud script *)

module WebApp

open System
open Suave
open Suave.Http
open Suave.Filters
open Suave.Operators
open Suave.Successful
open Suave.Utils.Collections
open System.Net
open System.Security.Cryptography

(*let readData k q =*)
  (*let lg = sprintf "q= %A" q*)
  (*match q ^^ k with *)
  (*| Choice1Of2 x -> sprintf "%s - Log=%s" x lg*)
  (*| Choice2Of2 x -> sprintf "NOTFOUND: %s - Log=%s" x lg*)

(*type CmdArgs = { IP: System.Net.IPAddress; Port: Sockets.Port }*)

(*let config = *)
    (*let port = System.Environment.GetEnvironmentVariable("PORT")*)
    (*let ip127  = IPAddress.Parse("127.0.0.1")*)
    (*let ipZero = IPAddress.Parse("0.0.0.0")*)

    (*{ defaultConfig with *)
        (*bindings=[ (if port = null then HttpBinding.create HTTP ip127 (uint16 8080)*)
                    (*else HttpBinding.create HTTP ipZero (uint16 port)) ] }*)

// Domain types for the PHM Web Server
// Internal error type
type PHMError =
  | PHMErrMsg of string

// Manage a Result type
type Result<'a, 'b> =
  | Ok of 'a
  | Error of 'b

// Internal PHM Result type
type PHMResult = Result<string, PHMError>

// Internal PHM Message type with Reply channel
type PHMmsg = AsyncReplyChannel<string * PHMResult>

// messages for the inner MailboxProcessor
type pMSG =
  | Send of string * string * PHMmsg
  | Get of string * PHMmsg

// Internal MailboxProcessor
type MessageCore() =
  let agent = MailboxProcessor<pMSG>.Start(fun inbox ->
      let rec msgLoop fkDb = 
        async {
          let! msg = inbox.Receive()
          match msg with
          | Send (vin, data, r) -> 
                  let bytes = data 
                              |> System.Text.Encoding.ASCII.GetBytes 
                              |> HashAlgorithm.Create("SHA256").ComputeHash
                  let hashed = sprintf "%s-%A" vin bytes
                  // send it ...
                  let newmap = fkDb |> (Map.add vin hashed)
                  r.Reply(vin, (PHMResult.Ok hashed))
                  return! msgLoop newmap
          | Get (vin, r) -> 
                  let r1 =
                    match (fkDb |> Map.tryFind vin) with
                    | Some d -> Ok d
                    | _ -> Error (vin |> sprintf "No information found with %s VIN number" |> PHMErrMsg)
                  
                  r.Reply(vin, r1)
                  return! msgLoop fkDb
        }

      msgLoop Map.empty
    )

  member this.GetData vin = 
    agent.PostAndAsyncReply (fun r -> pMSG.Get (vin, r))

  member this.SendData vin data = 
    agent.PostAndAsyncReply (fun r -> pMSG.Send (vin, data, r))

// Computation expression for the Option
type MaybeBuilder () = 
  member this.Bind (e, f) =
    match e with 
    | Some x -> x |> f
    | _ -> None

  member this.Return x = Some x

  member this.ReturnFrom x = x

  member this.Zero () = this.Return ()


let choice2Option =
  function
  | Choice1Of2 x -> Some x 
  | Choice2Of2 _ -> None

let phmRes2Option (str, (phmR: PHMResult)) = 
  match phmR with 
  | Ok s -> Some s
  | _ -> None
  

let getMessage ctx =
  async {
    let html = "<ul><li><strong>System:</strong> Hello PHM!</li></ul>"
    return! Successful.OK html ctx
  }

let findKey q key =
  q ^^ key

// get the head of the list or return None
let queryFirst q = 
  match q with
  | x :: xs -> fst x |> Some
  | _ -> None

let logger2Console h m = 
  printfn "%s: %A ***" h m

// parse the request from the web
// Receive a tuple with the Id (VIN number) and the list of params of the request
// We use unamed params and getting the first from the request.
// This is an example of the proper request (with curl):
//   curl -H "Content-Type: application/json" -X POST -d '{...<data>...}' <server URL>
let reqPHMParser req =
  let vinId, qData = req
  let logP =  logger2Console "reqPHMParser"
  (sprintf "id=%s q=%A" vinId qData) |> logP
  let mb = new MaybeBuilder()   
  let messenger = new MessageCore()
  mb {   // monadic expression
    let! js  = qData |> queryFirst
    let! ret = 
      async {
        return! (messenger.SendData vinId js)
      } |> Async.RunSynchronously   // run the async call
        |> phmRes2Option            // convert from result to option
    return ret                      // return the result
  }


let app =
  choose
    [ GET >=> choose
        [ path "/" >=> OK "Hello PHM"
          path "/hello" >=> OK "Hello GET"
          path "/goodbye" >=> OK "Good bye GET" ]
      POST >=> choose
        [ path "/" >=> OK "PHM POST"
          path "/hello" >=> OK "Hello POST"
          pathScan "/data/%s" (
                  fun id -> 
                    request (
                      fun r -> 
                        match ((id, r.form) |> reqPHMParser) with
                        | Some x -> OK x
                        | _ -> OK "Request error"
                    )
                  )  ]
    ]
    // POST >=> request (fun r -> OK (r.rawForm |> sprintf "rawform %A") )
