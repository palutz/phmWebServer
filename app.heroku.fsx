// --------------------------------------------------------------------------------------
// Start the 'app' WebPart defined in 'app.fsx' on Heroku using %PORT%
// --------------------------------------------------------------------------------------

#load "app.fsx"
open App
open System
open Suave
open System.Net


type CmdArgs = { IP: System.Net.IPAddress; Port: Sockets.Port }

let config = 
  let port = System.Environment.GetEnvironmentVariable("PORT")
  let ip127  = IPAddress.Parse("127.0.0.1")
  let ipZero = IPAddress.Parse("0.0.0.0")

  { defaultConfig with 
      bindings=[ (if port = null then HttpBinding.create HTTP ip127 (uint16 8080)
                  else HttpBinding.create HTTP ipZero (uint16 port)) ] }

(*let serverConfig =*)
  (*let port = int (Environment.GetEnvironmentVariable("PORT"))*)
  (*{ Web.defaultConfig with*)
      (*homeFolder = Some __SOURCE_DIRECTORY__*)
      (*// logger = Logging.Loggers.saneDefaultsFor Logging.LogLevel.Warn*)
      (*bindings = [ HttpBinding.create HTTP IPAddress"0.0.0.0" port ] }*)
  

Web.startWebServer config app
