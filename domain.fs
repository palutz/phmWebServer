namespace PHM.Data

module Domain =
  (*open System.Runtime.Serialization*)
  (*[<DataContract>]*)
  (*type CarData =*)
    (*{ *)
      (*[<field: DataMember(Name = "vin")>]*)
      (*vin : string*)
      (*[<field: DataMember(Name = "RegNum")>]*)
      (*RegNumber : string*)
      (*[<field: DataMember(Name = "data")>]*)
      (*data : string*)
    (*}*)

  (*type Bar = *)
    (*{*)
      (*bar : string *)
    (*}*)

  let choice2Option c =
    match c with 
    | Choice1Of2 x -> Some x 
    | Choice2Of2 _ -> None

  type MaybeBuilder () = 
    member this.Bind (e, f) =
      match e with 
      | Some x -> x |> f
      | _ -> None

    member this.Return x =
      Some x

    member this.ReturnFrom x = x
