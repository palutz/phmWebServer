#if BOOTSTRAP
  System.Environment.CurrentDirectory <- __SOURCE_DIRECTORY__
  if not (System.IO.File.Exists "paket.exe") then
    let url = "https://github.com/fsprojects/Paket/releases/download/5.122.0/paket.exe" in use wc = new System.Net.WebClient() in let tmp = System.IO.Path.GetTempFileName() in wc.DownloadFile(url, tmp); System.IO.File.Move(tmp,System.IO.Path.GetFileName url);;

  #r "paket.exe"
  Paket.Dependencies.Install (System.IO.File.ReadAllText "paket.dependencies")
#endif

//---------------------------------------------------------------------

#I "packages/Suave/lib/net40"
#r "packages/Suave/lib/net40/Suave.dll"
#load "jsonParser.fs"


open System
open Suave
open Suave.Filters
open Suave.Operators
open Suave.Successful
open Suave.Utils.Collections
open System.Net
open System.Threading

open JsonParser

(*let readData k q =*)
  (*let lg = sprintf "q= %A" q*)
  (*match q ^^ k with *)
  (*| Choice1Of2 x -> sprintf "%s - Log=%s" x lg*)
  (*| Choice2Of2 x -> sprintf "NOTFOUND: %s - Log=%s" x lg*)

(*type CmdArgs = { IP: System.Net.IPAddress; Port: Sockets.Port }*)

(*let config = *)
    (*let port = System.Environment.GetEnvironmentVariable("PORT")*)
    (*let ip127  = IPAddress.Parse("127.0.0.1")*)
    (*let ipZero = IPAddress.Parse("0.0.0.0")*)

    (*{ defaultConfig with *)
        (*bindings=[ (if port = null then HttpBinding.create HTTP ip127 (uint16 8080)*)
                    (*else HttpBinding.create HTTP ipZero (uint16 port)) ] }*)
let choice2Option c =
  match c with 
  | Choice1Of2 x -> Some x 
  | Choice2Of2 _ -> None

type MaybeBuilder () = 
  member this.Bind (e, f) =
    match e with 
    | Some x -> x |> f
    | _ -> None

  member this.Return x =
    Some x

  member this.ReturnFrom x = x

let getMessage ctx =
  async {
    let html = "<ul><li><strong>System:</strong> Hello PHM!</li></ul>"
    return! Successful.OK html ctx
  }
  
let choiceLog q key =
  let v = q ^^ key |> choice2Option
  printfn "%s=%A" key v
  v

// get the head of the list or return None
//let lQueryLog = 
//  function
//    | x :: xs -> printfn "%s=%A" x; Some x 
//    | _ -> None

let reqPHMParser q =
  // defaultArg (Option.ofChoice (q ^^ "name")) "World" |> sprintf "Hello %s"
  // same code.. without the Option.ofChoice
  //match q ^^ "name" with 
  //| Choice1Of2 x -> x
  //| Choice2Of2 y -> "World"
  //|> sprintf "Hello %s"S

  printfn "q = %A" q
  let mb = new MaybeBuilder()
  mb {
    //let! vin = "vin" |> choiceLog q
    //let! reg = "regnum" |> choiceLog q
    //let! js  = "data" |> choiceLog q
    let! rq = "data" |> choiceLog q
    let! js = rq |> jsonParseIt
    //return (sprintf "Vin: %s\nRegNum: %s\ndata: %s" vin reg js)
    return sprintf "js = %A" js
  } |> function
      | Some r -> r
      | _ -> "Not a valid JSON"


let app =
  choose
    [ GET >=> choose
        [ path "/" >=> OK "Hello PHM"
          path "/hello" >=> OK "Hello GET"
          path "/goodbye" >=> OK "Good bye GET" ]
      POST >=> choose
        [ path "/" >=> OK "PHM POST"
          path "/hello" >=> OK "Hello POST"
          path "/data" >=> request (fun r -> OK (reqPHMParser r.form)) ] ]

    // POST >=> request (fun r -> OK (r.rawForm |> sprintf "rawform %A") )

[<EntryPoint>]
let main argv = 
  let cts = new CancellationTokenSource()
  let conf = { defaultConfig with cancellationToken = cts.Token }
  let listening, server = startWebServerAsync conf app
    
  Async.Start(server, cts.Token)
  printfn "Make requests now"
  Console.ReadKey true |> ignore
    
  cts.Cancel()

  0 // return an integer exit code
