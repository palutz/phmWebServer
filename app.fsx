#if BOOTSTRAP
  System.Environment.CurrentDirectory <- __SOURCE_DIRECTORY__
  if not (System.IO.File.Exists "paket.exe") then
    let url = "https://github.com/fsprojects/Paket/releases/download/5.122.0/paket.exe" in use wc = new System.Net.WebClient() in let tmp = System.IO.Path.GetTempFileName() in wc.DownloadFile(url, tmp); System.IO.File.Move(tmp,System.IO.Path.GetFileName url);;

  #r "paket.exe"
  Paket.Dependencies.Install (System.IO.File.ReadAllText "paket.dependencies")
#endif

//---------------------------------------------------------------------

// #load "jsonParser.fs"
#I "packages/Suave/lib/net40"
#r "packages/Suave/lib/net40/Suave.dll"

open System
open Suave
open Suave.Http
open Suave.Filters
open Suave.Operators
open Suave.Successful
open Suave.Utils.Collections
open System.Net
open System.Security.Cryptography

(*let readData k q =*)
  (*let lg = sprintf "q= %A" q*)
  (*match q ^^ k with *)
  (*| Choice1Of2 x -> sprintf "%s - Log=%s" x lg*)
  (*| Choice2Of2 x -> sprintf "NOTFOUND: %s - Log=%s" x lg*)

(*type CmdArgs = { IP: System.Net.IPAddress; Port: Sockets.Port }*)

(*let config = *)
    (*let port = System.Environment.GetEnvironmentVariable("PORT")*)
    (*let ip127  = IPAddress.Parse("127.0.0.1")*)
    (*let ipZero = IPAddress.Parse("0.0.0.0")*)

    (*{ defaultConfig with *)
        (*bindings=[ (if port = null then HttpBinding.create HTTP ip127 (uint16 8080)*)
                    (*else HttpBinding.create HTTP ipZero (uint16 port)) ] }*)

type PHMError =
  | PHMErrMsg of string

type Result<'a, 'b> =
  | Ok of 'a
  | Error of 'b

type PHMResult = Result<string, PHMError>

type PHMmsg = AsyncReplyChannel<string * PHMResult>

type pMSG =
  | Send of string * string * PHMmsg
  | Get of string * PHMmsg

type MessageCore() =
  let agent = MailboxProcessor<pMSG>.Start(fun inbox ->
      let rec msgLoop fkDb = 
        async {
          let! msg = inbox.Receive()
          match msg with
          | Send (vin, data, r) -> 
                  let bytes = data 
                              |> System.Text.Encoding.ASCII.GetBytes 
                              |> HashAlgorithm.Create("SHA256").ComputeHash
                  let hashed = sprintf "%s-%A" vin bytes
                  // send it ...
                  let newmap = fkDb |> (Map.add vin hashed)
                  r.Reply(vin, (PHMResult.Ok hashed))
                  return! msgLoop newmap
          | Get (vin, r) -> 
                  let r1 =
                    match (fkDb |> Map.tryFind vin) with
                    | Some d -> Ok d
                    | _ -> Error (vin |> sprintf "No information found with %s VIN number" |> PHMErrMsg)
                  
                  r.Reply(vin, r1)
                  return! msgLoop fkDb
        }

      msgLoop Map.empty
    )

  member this.GetData vin = 
    agent.PostAndAsyncReply (fun r -> pMSG.Get (vin, r))

  member this.SendData vin data = 
    agent.PostAndAsyncReply (fun r -> pMSG.Send (vin, data, r))


type MaybeBuilder () = 
  member this.Bind (e, f) =
    match e with 
    | Some x -> x |> f
    | _ -> None

  member this.Return x = Some x

  member this.ReturnFrom x = x

  member this.Zero () = this.Return ()


let choice2Option =
  function
  | Choice1Of2 x -> Some x 
  | Choice2Of2 _ -> None

let phmRes2Option (str, (phmR: PHMResult)) = 
  match phmR with 
  | Ok s -> Some s
  | _ -> None
  

let getMessage ctx =
  async {
    let html = "<ul><li><strong>System:</strong> Hello PHM!</li></ul>"
    return! Successful.OK html ctx
  }

let findKey q key =
  q ^^ key

// get the head of the list or return None
let queryFirst q = 
  match q with
  | x :: xs -> fst x |> Some
  | _ -> None

let logger2Console h m = 
  printfn "%s: %A ***" h m

let reqPHMParser req =
  let vinId, qData = req
  let logP =  logger2Console "reqPHMParser"
  (sprintf "id=%s q=%A" vinId qData) |> logP
  let mb = new MaybeBuilder()
  let messenger = new MessageCore()
  mb {
    let! js  = qData |> queryFirst
    let! ret = 
      async {
        return! (messenger.SendData vinId js)
      } |> Async.RunSynchronously 
        |> phmRes2Option
    return ret
  }


let app =
  choose
    [ GET >=> choose
        [ path "/" >=> OK "Hello PHM"
          path "/hello" >=> OK "Hello GET"
          path "/goodbye" >=> OK "Good bye GET" ]
      POST >=> choose
        [ path "/" >=> OK "PHM POST"
          path "/hello" >=> OK "Hello POST"
          pathScan "/data/%s" (
                  fun id -> 
                    request (
                      fun r -> 
                        match ((id, r.form) |> reqPHMParser) with
                        | Some x -> OK x
                        | _ -> OK "Request error"
                    )
                  )  ]
    ]
