module JsonParser
//namespace PHM.Data
  type token =
    | WhiteSpace
    | Symbol of char
    | StrToken of string
    | NumToken of float
    | BoolToken of bool

  let (|Match|_|) pattern input =
    let m = System.Text.RegularExpressions.Regex.Match(input, pattern)
    match m.Success with 
    | true -> Some m.Value
    | _ -> None

  let bool = System.Boolean.Parse
  let unquote (s:string) = s.Substring(1,s.Length-2)

  let toToken = function
    | Match @"^\s+" s -> s, WhiteSpace
    | Match @"^""[^""\\]*(?:\\.[^""\\]*)*""" s -> s, s |> unquote |> StrToken
    | Match @"^\{|^\}|^\[|^\]|^:|^," s -> s, s.[0] |> Symbol
    | Match @"^\d+(\.\d+)?|\.\d+" s -> s, s |> float |> NumToken
    | Match @"^true|false" s -> s, s |> bool |> BoolToken
    | _ -> invalidOp "Unknown token"

  let tokenize s =
    let rec tokenize' index (s:string) =
      if index = s.Length then []
      else
        let next = s.Substring index 
        let text, token = toToken next
        token :: tokenize' (index + text.Length) s
    tokenize' 0 s
    |> List.choose (function WhiteSpace -> None | t -> Some t)

  type json =
    | JNumber of float
    | JString of string
    | JBoolean of bool
    | JArray of json list
    | JObject of (string * json) list
    | JNull

  let rec (|ValueRec|_|) = function
    | NumToken n::t -> Some(JNumber n, t)
    | BoolToken b::t -> Some(JBoolean b, t)
    | StrToken s::t -> Some(JString s, t)
    | Symbol '['::ValuesRec(vs, Symbol ']'::t) -> Some(JArray vs,t)
    | Symbol '{'::PairsRec(ps, Symbol '}'::t) -> Some(JObject ps,t)
    | [] -> Some(JNull,[])
    | _ -> None
  and (|ValuesRec|_|) = function
    | ValueRec(p,t) ->
        let rec aux p' = function
            | Symbol ','::ValueRec(p,t) -> aux (p::p') t
            | t -> p' |> List.rev,t
        aux [p] t |> Some
    | _ -> None
  and (|PairRec|_|) = function
    | StrToken k::Symbol ':'::ValueRec(v,t) -> Some((k,v), t)
    | _ -> None
  and (|PairsRec|_|) = function
    | PairRec(p,t) ->
      let rec aux p' = function
        | Symbol ','::PairRec(p,t) -> aux (p::p') t
        | t -> p' |> List.rev,t
      aux [p] t |> Some
    | _ -> None

  let jsonParseIt s =
    tokenize s |> function 
    | ValueRec(v,[]) -> Some v
    | _ -> None // "Failed to parse JSON"

(*module Test =*)
  (*open JsonParser*)

  (*let jsonString = sprintf "{\"Name\": \"%s\", \"Phone\": %s}" "cippalippa" "0123456789"*)
  (*let person = parse jsonString*)
  (*let name, phone = person |> function*)
      (*| JObject(["Name", JString name; "Phone", JNumber phone]) ->*)
          (*name, phone*)
      (*| _ -> invalidOp "Invalid object"*)
